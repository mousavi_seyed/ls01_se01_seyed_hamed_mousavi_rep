
public class Aufgabe2 {

	public static void main(String[] args) {
		System.out.printf("%-5s=", "0!");
		System.out.printf("%-19s=", "");
		System.out.printf("%-3s1%n", "");
		
		
		System.out.printf("%-5s=", "1!");
		System.out.printf("%-19s=", "1");
		System.out.printf("%-3s1%n", "");
		
		
		System.out.printf("%-5s=", "2!");
		System.out.printf("%-19s=", "1 * 2");
		System.out.printf("%-3s2%n", "");
		
		
		System.out.printf("%-5s=", "3!");
		System.out.printf("%-19s=", "1 * 2 * 3");
		System.out.printf("%-3s6%n", "");
		
		
		System.out.printf("%-5s=", "4!");
		System.out.printf("%-19s=", "1 * 2 * 3 * 4");
		System.out.printf("%-3s24%n","");
		
		
		System.out.printf("%-5s=", "4!");
		System.out.printf("%-19s=", "1 * 2 * 3 * 4 * 5");
		System.out.printf("%-3s120%n","");
	}

}
